/* Joonatan Kuosa
 *
 * Small app that reads QR codes and allows saving them.
 */
import React, { Component } from 'react';
//import react in our code.
import { Text, View, Linking, TouchableHighlight, Share, StyleSheet} from 'react-native';
import { Camera, } from 'expo-camera';
import * as Permissions from 'expo-permissions';

export default class App extends Component {
  state = {
    hasCameraPermission: null,
    qrvalue: '',
    openScanner: true,
  };

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  onOpenlink() {
    //Function to open URL, If scanned
    Linking.openURL(this.state.qrvalue);
    //Linking used to open the URL in any browser that you have installed
  }

  onBarcodeScan(evt) {
    val = evt.data
    //called after te successful scanning of QRCode/Barcode
    this.setState({ qrvalue: val });
    this.setState({ openScanner: false });
  }

  onOpenScanner() {
    this.setState({ qrvalue: '' });
    this.setState({ openScanner: true });
  }

  onShare = async () => {
    const value = this.state.qrvalue;
    try {
      const result = await Share.share({ message: value });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  // TODO test revoking app camera permissions after
  render() {
    //If qrvalue is set then return this view
    if (!this.state.openScanner) {
      return (
        <View style={styles.container}>
            <Text style={styles.heading}>QR Code Reader</Text>
            <Text style={styles.simpleText}>{this.state.qrvalue ? 'Scanned QR Code: '+ this.state.qrvalue : ''}</Text>
            {this.state.qrvalue.includes("http") ?
              <TouchableHighlight
                onPress={() => this.onOpenlink()}
                style={styles.button}>
                  <Text style={{ color: '#FFFFFF', fontSize: 12 }}>Open Link</Text>
              </TouchableHighlight>
              : null
            }
            <TouchableHighlight
              style={styles.button}
              onPress={this.onShare}>
                <Text style={{ color: '#FFFFFF', fontSize: 12 }}>Share</Text>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={() => this.onOpenScanner()}
              style={styles.button}>
                <Text style={{ color: '#FFFFFF', fontSize: 12 }}>Scan</Text>
            </TouchableHighlight>
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <Camera
          style={{ flex: 1 }}
          onBarCodeScanned={event => this.onBarcodeScan(event)}
          type={Camera.Constants.Type.back}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              flexDirection: 'row',
            }}>
          </View>
        </Camera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'white'
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#2c3539',
    padding: 10,
    width:300,
    marginTop:16
  },
  heading: {
    color: 'black',
    fontSize: 24,
    alignSelf: 'center',
    padding: 10,
    marginTop: 30
  },
  simpleText: {
    color: 'black',
    fontSize: 20,
    alignSelf: 'center',
    padding: 10,
    marginTop: 16
  }
});
